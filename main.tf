
provider "kubernetes" {
  config_path = "~/.kube/config"
}

provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
  }
}

resource "helm_release" "nginx-ingress" {
  name            = "nginx-ingress"
  repository      = "https://kubernetes.github.io/ingress-nginx"
  chart           = "ingress-nginx"
  cleanup_on_fail = true
}

resource "helm_release" "ingress-controller" {
  name            = "ingress-controller"
  chart           = "./ingress-chart"
  cleanup_on_fail = true

}

resource "random_password" "rootPassword" {
  length = 16
}

resource "random_password" "sonarqubePassword" {
  length = 16
}

resource "kubernetes_secret" "sonarSecrets" {
  metadata {
    name = "sonarsecrets"
  }

  data = {
    "mysqlRootPassword" = random_password.rootPassword.result
    "sonarqubePassword" = random_password.sonarqubePassword.result
  }
}

resource "helm_release" "mysql" {
  name            = "mysql"
  chart           = "./mysql-chart"
  cleanup_on_fail = true
}

resource "helm_release" "sonarqube" {
  name            = "sonarqube"
  chart           = "./sonarqube"
  force_update    = true
  cleanup_on_fail = true

}
